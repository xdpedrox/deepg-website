
import React, {Component} from 'react';


import axios from '../../../axios';
import Input from '../../../components/Ui/Input/Input';
import Button from '../../../components/Ui/Button/Button'
import Spinner from '../../../components/Ui/Spinner/Spinner'
import _ from 'lodash';

import classes from './CreateTeam.module.css';

// import ReCAPTCHA from "react-google-recaptcha";

class CreateTeam extends Component {
    state = {
        orderForm: {
            channelName: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Nome do canal'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 3,
                    maxLength: 10,
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password do canal'
                },
                value: '',
                validation: {
                    required: false
                },
                valid: false,
                touched: false
            },
            // zipCode: {
            //     elementType: 'input',
            //     elementConfig: {
            //         type: 'text',
            //         placeholder: 'ZIP Code'
            //     },
            //     value: '',
            //     validation: {
            //         required: true,
            //         minLength: 5,
            //         maxLength: 5,
            //         isNumeric: true
            //     },
            //     valid: false,
            //     touched: false
            // },
            // country: {
            //     elementType: 'input',
            //     elementConfig: {
            //         type: 'text',
            //         placeholder: 'Country'
            //     },
            //     value: '',
            //     validation: {
            //         required: true
            //     },
            //     valid: false,
            //     touched: false
            // },
            // email: {
            //     elementType: 'input',
            //     elementConfig: {
            //         type: 'email',
            //         placeholder: 'Your E-Mail'
            //     },
            //     value: '',
            //     validation: {
            //         required: true,
            //         isEmail: true
            //     },
            //     valid: false,
            //     touched: false
            // },
            // gameArea: {
            //     elementType: 'select',
            //     elementConfig: {
            //         options: [
            //             {value: '1', displayValue: 'CSGO'},
            //             {value: '2', displayValue: 'LOL'}
            //         ]
            //     },
            //     value: '',
            //     validation: {},
            //     valid: true
            // }
        },
        formIsValid: false,
        loading: false,
        isHuman: true
    }

    callback () {
        this.setState({isHuman: true})
    };


    submitHandler = (event) => {
        event.preventDefault();
        if (this.state.isHuman & this.state.orderForm.channelName.touched) {
            this.setState({loading: true});
            const formData = {};
            for (let formElementIdentifier in this.state.orderForm) {
                formData[formElementIdentifier] = this.state.orderForm[formElementIdentifier].value;
            }

            console.log(formData)
            // const order = {
            //     ingredients: this.props.ingredients,
            //     price: this.props.price,
            //     orderData: formData
            // }

            axios.put('team/team', {
                name: formData.channelName,
                password: formData.password,
                areaId: "5bc7423e975a8d388e71a9b4",
                move: true
            })
                .then(response => {
                    this.setState({loading: false});
                })
                .catch(error => {
                    this.setState({loading: false});
                });
        }
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = !_.isEqual(value.trim(),  '') && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, inputIdentifier) => {
        const updatedOrderForm = {
            ...this.state.orderForm
        };
        const updatedFormElement = {
            ...updatedOrderForm[inputIdentifier]
        };
        updatedFormElement.value = event.target.value;
        updatedFormElement.valid = this.checkValidity(updatedFormElement.value, updatedFormElement.validation);
        updatedFormElement.touched = true;
        updatedOrderForm[inputIdentifier] = updatedFormElement;

        let formIsValid = true;
        for (let inputIdentifier in updatedOrderForm) {
            formIsValid = updatedOrderForm[inputIdentifier].valid && formIsValid;
        }
        this.setState({orderForm: updatedOrderForm, formIsValid: formIsValid});
    }

    render() {




        const formElementsArray = [];
        for (let key in this.state.orderForm) {
            formElementsArray.push({
                id: key,
                config: this.state.orderForm[key]
            });
        }
        let form = (
            <form onSubmit={this.submitHandler}>
                {formElementsArray.map(formElement => (
                    <Input
                        key={formElement.id}
                        elementType={formElement.config.elementType}
                        elementConfig={formElement.config.elementConfig}
                        value={formElement.config.value}
                        invalid={!formElement.config.valid}
                        shouldValidate={formElement.config.validation}
                        touched={formElement.config.touched}
                        changed={(event) => this.inputChangedHandler(event, formElement.id)}/>
                ))}



                <Button btnType="Success" disabled={!this.state.formIsValid}>Submit</Button>
            </form>

        );
        if (this.state.loading) {
            form = <Spinner/>;
        }

        return (
            <div className={classes.CreateTeam}>
                <h4>Criar um Canal</h4>
                {form}




            </div>
        );
    }
}

export default CreateTeam;




// import React, { Component } from 'react';
// import { TextInput, Button, Avatar } from 'evergreen-ui'

// import classes from './CreateTeam.module.css'


// class CreateTeam extends Component {
//     render() {
//         return (
//             <div className={classes.CreateTeam}>
//                 <br />
//                 <br />
//                 <h3>PERFIL</h3>
//                 <Avatar
//                     src="https://upload.wikimedia.org/wikipedia/commons/a/a1/Alan_Turing_Aged_16.jpg"
//                     name="Alan Turing"
//                     size={120}
//                 />
//                 < br />
//                 Nome: <div className={classes.vermelho}>Alan Truring</div> Equipa: <div className={classes.vermelho}>DeepGaming</div>
//                 Tempo online: <div className={classes.vermelho}>1 Ano</div>
//                 Rank: <div className={classes.vermelho}>Super Admin</div>
//                 Tempo para o próximo nível:

//                 < br />
//                 <h3>CRIAR CANAL</h3>
//                 Nome do canal:
//                 <TextInput
//                     name="texto"
//                     placeholder="Nome do canal"
//                 />
//                 < br />
//                 Password do canal:
//                 <TextInput
//                     name="texto"
//                     placeholder="Password do canal"
//                 />
//                 < br />
//                 O teu nome:
//                 <TextInput
//                     name="texto"
//                     placeholder="O teu nome"
//                 />
//                 < br />
//                 E-mail:
//                 <TextInput
//                     name="texto"
//                     placeholder="O teu E-mail"
//                 />
//                 <br />
//                 <br />
//             </div>
//         );
//     }
// }

// export default CreateTeam;