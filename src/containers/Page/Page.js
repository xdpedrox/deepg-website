import React, {Component} from 'react';

import axios from '../../axios';

import classes from './Page.module.css';
import Header from '../../hoc/Header/Header';
import Nav from '../../hoc/Nav/Nav';
import Footer from '../../hoc/Footer/Footer';

import {NavLink, Route, Switch} from 'react-router-dom'
import Home from './Home/Home'
import Teams from './Teams/Teams'
import Team from './Team/Team'
import Groups from './Groups/Groups'
import ManageTeam from "./ManageTeam/ManageTeam";
import CreateTeam from "./CreateTeam/CreateTeam";
import Member from "./Member/Member";
import Members from "./Members/Members";
import MemberDashboard from "./MemberDashboard/MemberDashboard";
import AdminPage from "./AdminPage/AdminPage";
import Regras from "./Regras/Regras";


class Page extends Component {

    state = {
        member: {
            _id: 1
        }

    }

    componentDidMount() {
        //GET Team TODO move to higher component
        axios.put('/auth/me')
            .then(response => {

                let memberId = response.data.memberId;
                let responseData = response.data.server;


                // console.log(memberId);
                this.setState({
                    member: {_id: memberId}
                })
                // this.setState({teams: updatedPosts});
                // console.log(this.state);


            })
            .catch(error => {
                console.log(error);
            });


    }

    render() {

        return (
            <div className={classes.PageSize}>
                <Header />
                <Nav />
                <Switch className={classes.PageContent}>
                    {/* Paginas estaticas */}
                    <Route path="/regras" exact component={Regras}/>
                    {/*Main Routes*/}
                    
                    <Route path="/admin" exact component={AdminPage}/>
                    <Route path="/" exact component={Home}/>

                    {/*TeamRoutes*/}
                    <Route path="/team/create" exact component={CreateTeam}/>
                    <Route path="/team" exact component={Teams}/>
                    <Route path="/team/dashboard/:teamId/" component={ManageTeam}/>
                    <Route path="/team/:teamId"
                           render={(props) => <Team {...props} loginMemberId={this.state.member._id}/>}/>


                    {/*MemberRoutes*/}
                    <Route path="/member" exact component={Members}/>
                    <Route path="/member/dashboard" exact component={MemberDashboard}/>
                    <Route path="/member/:memberId" component={Member}/>

                    {/*Groups*/}
                    <Route path="/groups" exact component={Groups}/>
                </Switch>
                <Footer />

            </div>
        );
    }

}

export default Page;
