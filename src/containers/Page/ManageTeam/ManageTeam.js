import React, {Component} from 'react';

class ManageTeam extends Component {

    state = {
        status: 100
    }

    componentDidMount() {
        this.setState({
            status: 200
        })
    }

    //Checks if the Page is Loaded Properly
    generatePage() {
        let page = (<div></div>);

        if (this.state.status == 404) {
            page = (<h1>Team not Found - 404</h1>)
        } else {
            page = (<div></div>

            );
        }


        return page;
    }

    render() {

        return (
            <div>
                <h1>ManageTeam</h1>
                {this.generatePage()}
            </div>

        );
    }


}

export default ManageTeam;