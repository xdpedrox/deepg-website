import React, {Component} from 'react';
import Aux from "../Team/Team";

class Member extends Component {

    state = {
        status: 100
    }

    componentDidMount() {
        this.setState({
            status: 200
        })
    }

    //Checks if the Page is Loaded Properly
    generatePage() {
        let page = (<div></div>);

        if (this.state.status == 404) {
            page = (<h1>Member not Found - 404</h1>)
        } else {
            page = (<div></div>

            );
        }


        return page;
    }

    render() {

        return (
            <div>
                <h1>Member</h1>
                {this.generatePage()}
            </div>

        );
    }




}

export default Member;
