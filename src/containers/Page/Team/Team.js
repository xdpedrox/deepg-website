import React, {Component} from 'react';

import axios from '../../../axios';

import Aux from '../../../hoc/Auxx/Auxx';
import TeamInfo from '../../../components/TeamInfo/TeamInfo'
import ChannelViewer from '../../../components/ChannelViewer/ChannelViewer'
import _ from 'lodash';


import './Team.css';


class Team extends Component {

    state = {
        status: 100,
        team: {
            _id: this.props.match.params.teamId,
            serverGroupId: null,
            status: "",
            teamName: "",
            ownerID: "",
            channelOrder: 0,
            spacerNumber: 0,
            areaId: "",
            mainChannelId: 190,
            spacerEmptyId: 0,
            spacerBarId: 0,
            members: [],
            numberOfMember: 0
        },
        teamUrl: 'teamUrl',
        channels: [],


    }

    calculateNumberOfMember(responseData) {
        return Object.assign({numberOfMember: responseData.members.length}, responseData);
    }

    loadChannels () {

        //Get Users in the Channel
        axios.get('/teamspeak/channelview/' + this.state.team.mainChannelId)
            .then(response => {

                let responseStatus = response.data.status;
                let responseData = response.data.server;

                if (_.isEqual(responseStatus, "success")) {
                    this.setState({channels: responseData})
                    // console.log(this.state);
                    // console.log(this.props.loginMemberId)
                }

                // console.log(this.state);
            })
            .catch(error => {
                console.log(error);
                // this.setState({error: true});
            });
    }

    loadData () {
        //get Team
        axios.get('/team/team/' + this.state.team._id)
            .then(response => {

                let responseStatus = response.data.status;
                let responseData = response.data.server;

                if (_.isEqual(responseStatus, "success")) {
                    this.setState({
                        status: 200,
                        team: this.calculateNumberOfMember(responseData)
                    })

                    this.loadChannels()


                } else {
                    console.log("something Bad happened in the server")
                }

            })
            .catch(error => {
                if (error.response) {
                    if (error.response.status) {
                        this.setState({
                            status: 404
                        })
                    }
                }
                // if (error)
            });
    }


    componentDidMount() {
        this.loadData()
    }

    changeUserPermissions(memberId, permission) {
        // Get Users in the Channel
        axios.post('/team/member/' + this.state.team._id, {
            memberId: memberId,
            permission: permission
        })
            .then(response => {
                console.log(response)

                let responseStatus = response.data.status;
                let responseData = response.data.server;

                if (_.isEqual(responseStatus, "success")) {
                    this.setState({team: this.calculateNumberOfMember(responseData)})
                } else {
                    // Error handling
                }
                // console.log(this.state);
            })
            .catch(error => {
                console.log(error);
                // this.setState({error: true});
            });
        // console.log()

    }

    promoteUser(memberId) {
        //Get the member that we want to add in question
        if (this.state.team.members) {
            let member = this.state.team.members.filter(member => _.isEqual(member.memberId, memberId));


            if (_.isEmpty(member)) {
                this.changeUserPermissions(memberId, 4)
            } else {
                this.changeUserPermissions(memberId, member[0].permissions - 1)
            }
        }
    }

    demoteUser(memberId) {
        //Get the member that we want to add in question
        if (this.state.team.members) {
            let member = this.state.team.members.filter(member => _.isEqual(member.memberId, memberId));

            if (!_.isEmpty(member)) {
                this.changeUserPermissions(memberId, member[0].permissions + 1)
            }
        }
    }

    createServerGroup() {

        console.log(this.props)
        if (_.isNull(this.state.team.serverGroupId)) {
            // Get Users in the Channel
            axios.patch('/team/servergroup/' + this.state.team._id)
                .then(response => {
                    console.log(response)

                    let responseStatus = response.data.status;
                    let responseData = response.data.server;

                    if (_.isEqual(responseStatus, "success")) {
                        this.setState({team: this.calculateNumberOfMember(responseData)})
                    } else {
                        // Error handling
                    }

                })
                .catch(error => {
                    console.log(error);
                    // this.setState({error: true});
                });
            // console.log()
        } else {
            //Team already has a group Disable Button
        }
    }

    getUserPermission(memberId) {

        let member = this.state.team.members.filter(member => _.isEqual(member.memberId, memberId));

        if (_.isEmpty(member)) {
            return 5
        }

        return member[0].permissions;

    }

    //Checks if the Page is Loaded Properly
    generatePage() {
        let page = (<div></div>);

        if (this.state.status == 404) {
            page = (<h1>Team not Found - 404</h1>)
        } else {
            page = (
                <Aux>

                <h1>{this.state.team.teamName}</h1>
                <TeamInfo createServerGroup={this.createServerGroup.bind(this)} team={this.state.team}></TeamInfo>
                <ChannelViewer channels={this.state.channels} users={this.state.team.members}
                               demote={this.demoteUser.bind(this)}
                               promote={this.promoteUser.bind(this)}
                               loginMemberId={this.props.loginMemberId}
                               permission={this.getUserPermission.bind(this)}>{this.state.team.teamName}</ChannelViewer>
            </Aux>
            );
        }


        return page;
    }

    render() {

        return (
            <div>
                <h1>Channel Manage</h1>
                {this.generatePage()}
            </div>

        );
    }

}

export default Team;