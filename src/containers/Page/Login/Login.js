import React, {Component} from 'react';

import {
    Box,
    Button,
    CheckBox,
    Grommet,
    Form,
    // FormContext,
    FormField,
    RadioButton,
    RangeInput,
    Select,
    TextArea
} from "grommet";
import { grommet } from "grommet/themes";

const Login = () => (
    <Grommet theme={grommet}>

        <Box fill align="center" >
            <Box width="medium">
                <h1>Login</h1>
                <Form
                    onReset={event => console.log(event)}
                    onSubmit={({ value }) => console.log("Submit", value)}
                >
                    <FormField
                        label="Email:"
                        name="email"
                        required
                    />
                    <FormField
                        label="Password:"
                        name="password"
                        required
                        validate={{ regexp: /^[0-9]{6,16}$/, message: "4-6 digits" }}
                    />


                </Form>
            </Box>
        </Box>
    </Grommet>
);


export default Login;