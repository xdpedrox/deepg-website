import React from 'react';
import Aux from '../../../hoc/Auxx/Auxx'


import classes from './TeamLogo.module.css';




const teamLogo = (props) => {
    return (
        <Aux>    

            <img className={classes.Logo} src={props.logoUrl} alt={props.children} />
        </Aux>
    );


}

export default teamLogo;