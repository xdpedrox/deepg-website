import React, {Component} from 'react';


import classes from './Footer.module.css';

class Footer extends Component{
    // state = {
    // }
    


    render () {
        return (
            <div className={classes.Footer}>
                <p className={classes.CopyRight}>Copyright DeepGaming © {new Date().getFullYear()}</p> 
            </div>
        );
    }
}

export default Footer;