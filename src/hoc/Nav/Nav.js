import React, {Component} from 'react';
import {NavLink} from 'react-router-dom'

import Auxx from '../Auxx/Auxx';
import classes from './Nav.module.css';

class Nav extends Component{
    // state = {
    // }
    


    render () {
        return (
            <div className={classes.Nav}>
                {/* Paginas Estaticas */}
                <NavLink className={classes.NavLink} to="/regras" exact>Regras</NavLink>

                <NavLink className={classes.NavLink} to="/admin" exact>Admin</NavLink>
                <NavLink className={classes.NavLink} to="/" exact>Home</NavLink>

                {/*TeamRoutes*/}
                <NavLink className={classes.NavLink} to="/team/create" exact>Create Team</NavLink>
                <NavLink className={classes.NavLink} to="/team">Teams</NavLink>
                <NavLink className={classes.NavLink} to="/team" exact>Your Team</NavLink>
                <NavLink className={classes.NavLink} to="/team/dashboard/1" exact>ManageTeam</NavLink>


                {/*MemberRoutes*/}
                <NavLink className={classes.NavLink} to="/member" exact>Members</NavLink>
                <NavLink className={classes.NavLink} to="/member" exact>Member</NavLink>
                <NavLink className={classes.NavLink} to="/member/dashboard" exact>Member Settings</NavLink>

                {/*Groups*/}
                <NavLink className={classes.NavLink} to="/groups" exact>Groups</NavLink>
            </div>
        );
    }
}

export default Nav;