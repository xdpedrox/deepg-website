import React, {Component} from 'react';

import Aux from '../Auxx/Auxx';
import classes from './Layout.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';
import Header from '../Header/Header';
import Nav from '../Nav/Nav';
import Footer from '../Footer/Footer';

// Routes
import {NavLink, Route, Switch} from 'react-router-dom'
import Home from '../../containers/Page/Home/Home'
import Teams from '../../containers/Page/Teams/Teams'
import Team from '../../containers/Page/Team/Team'
import Groups from '../../containers/Page/Groups/Groups'
import ManageTeam from "../../containers/Page/ManageTeam/ManageTeam";
import CreateTeam from "../../containers/Page/CreateTeam/CreateTeam";
import Member from "../../containers/Page/Member/Member";
import Members from "../../containers/Page/Members/Members";
import MemberDashboard from "../../containers/Page/MemberDashboard/MemberDashboard";
import AdminPage from "../../containers/Page/AdminPage/AdminPage";

class Layout extends Component{
    state = {
        showSideDrawer: false
    }
    


    render () {
        return (
            <Aux>
                <Header className="header"></Header>
                <Nav className="Nav"></Nav>
                <Switch className={classes.PageContent}>
                    {/*Main Routes*/}
                    <Route path="/admin" exact component={AdminPage}/>
                    <Route path="/" exact component={Home}/>

                    {/*TeamRoutes*/}
                    <Route path="/team/create" exact component={CreateTeam}/>
                    <Route path="/team" exact component={Teams}/>
                    <Route path="/team/dashboard/:teamId/" component={ManageTeam}/>
                    <Route path="/team/:teamId"
                           render={(props) => <Team {...props} loginMemberId={this.state.member._id}/>}/>


                    {/*MemberRoutes*/}
                    <Route path="/member" exact component={Members}/>
                    <Route path="/member/dashboard" exact component={MemberDashboard}/>
                    <Route path="/member/:memberId" component={Member}/>

                    {/*Groups*/}
                    <Route path="/groups" exact component={Groups}/>
                </Switch>
                <Footer className="footer"></Footer>
            </Aux>
        );
    }
}

export default Layout;